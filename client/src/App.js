import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom'
// component
import Header from './components/Header'
import Footer from './components/Footer'
import Entrance from './components/Entrance'
import Home from './components/Home'
import Project from './components/Project'
import ProjectDetails from './components/ProjectDetails'
import About from './components/About'
import Contact from './components/Contact'
import Boudoir from './components/Boudoir'
import Hall from './components/Hall'
import Admin from './components/Admin'

function App() {
  return (
    <div className="App">

      <Router>
        {/* <Header /> */}
        <Switch>
          <Route path='/' component={Home} exact ></Route>
          <Route path='/project' component={Project} ></Route>
          <Route path='/project/:id' component={ProjectDetails} ></Route>
          <Route path='/boudoir' component={Boudoir} ></Route>
          <Route path='/hall' component={Hall} ></Route>
          <Route path='/about' component={About} ></Route>
          <Route path='/contact' component={Contact} ></Route>
          <Route path='/admin' component={Admin} ></Route>
          
        </Switch>
        {/* <Footer /> */}
      </Router>
      
        
    </div>
  );
}

export default App;
