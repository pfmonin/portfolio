import React from 'react'
import '../css/Home.css'
import Entrance from './Entrance'
import HomeScreen from './HomeScreen'

const Home = () => {
    return (
        <div className="home">

            <Entrance />

            <HomeScreen />
            
        </div>
    )
}

export default Home
