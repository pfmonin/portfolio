import React from 'react'
import '../css/Entrance.css'
import logo from '../img/logo.gif'


const Entrance = () => {
    return (
        <div className="entrance">
            <div className="gif-container">
                <img src={logo} alt=""/>
                {/* <iframe src={logo} width="480" height="480" frameBorder="0" class="giphy-embed" allowFullScreen></iframe> */}
            </div>
        </div>
    )
}

export default Entrance
