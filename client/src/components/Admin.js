import React, {useState, useEffect} from 'react'
import { Container, Button, Table, Modal, Form } from 'react-bootstrap'
import ProjectService  from '../service/ProjectService'
import { technosData } from '../data/Technos'
import axios from 'axios'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'


const Admin = props => {

    // Fetch projects
    const [projects , setProjects] = useState([])
    
    // Bootstrap
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    // formulaire
    const [title , setTitle] = useState("")
    const [subtitle, setSubtitle] = useState("")
    const [description, setDescription] = useState("")
    // const [techno, setTechno] = useState("")
    const [ technos, setTechnos ] = useState({})
    const [task, setTask] = useState("")    
    const [ tasks, setTasks ] = useState([])

    

    const animatedComponents = makeAnimated();

    //handle
    const handleSubmit = async(e) => {
        e.preventDefault();

        try {
            const project = { title, subtitle, description, technos }
            const newProject = await 
                axios
                    .post(
                        "http://localhost:8080/projects/create", 
                        project,
                        {headers : {
                            // 'x-auth-token' : token,
                            'Accept' : 'application/json',
                            'Content-Type': 'application/json'
                            }
                        }
                    )
                    .then(response => response.data) 
                    console.log(newProject)
        } catch (error) {
            console.log(error)
        }
    }



    // const  handleChange = async(e) => {this.setTechnos({value: e.target.value});  }
    


    // Hooks 
    const fetchProject = async() => {
        try {
            const data = await ProjectService.findAll()
            setProjects(data)
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        fetchProject()        
    }, [])

    return (
        <div className="admin">
            <Container>
                <h1>My project</h1>
                <Button variant="info" onClick={handleShow}>
                    <i className="fas fa-plus"></i>
                    Add new
                </Button>

                <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                    <th>Id</th>
                    <th>Title</th>
                    <th>Subtitle</th>
                    <th>description</th>
                    <th>Techno</th>
                    <th>Task</th>
                    <th></th>
                    </tr>
                </thead>
                <tbody>
                    { projects.map(p => (
                    <tr key={p._id}>

                        <>
                        <td> {p._id} </td>
                        <td> {p.title} </td>
                        <td> {p.subtitle} </td>
                        <td>{p.description}</td>
                        <td>
                            {p.techno.map( t => (
                                <img key={t._id} src={t.icon} alt={t.name} style={{width:"20px", borderRadius: "50%"}} />
                            ))}
                        </td>                        
                        <td>                            
                            {p.task.map( ta => (
                                <p key={ta._id}> {ta.title} </p>
                            ) )}                             
                        </td>
                        <td>
                            <Button variant="success"> <i className="fas fa-pen"></i> </Button>
                            <Button variant="danger"> <i className="fas fa-trash"></i> </Button>
                        </td>
                        
                        </>
                    
                    </tr>
                    ) ) }
                   
                </tbody>
                </Table>
            </Container>  

            {/* Modal */}
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add new project to list</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Form onSubmit={handleSubmit}>
                    <Form.Group controlId="formTitle">
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" placeholder="Enter project title" onChange={(e) => setTitle(e.target.value)} />                        
                    </Form.Group>

                    <Form.Group controlId="formSubtitle">
                        <Form.Label>Subtitle</Form.Label>
                        <Form.Control type="text" placeholder="Enter project subtitle" onChange={(e) => setSubtitle(e.target.value)} />                        
                    </Form.Group>

                    <Form.Group controlId="formDescription">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" placeholder="Enter project description" onChange={(e) => setDescription(e.target.value)} />                        
                    </Form.Group>

                    <Form.Group controlId="formTechno">
                        <Form.Label>Techno</Form.Label>
                        <Select 
                            closeMenuOnSelect={false}
                            components={animatedComponents}
                            isMulti={true}
                            name="technoName"                            
                            className="basic-multi-select"
                            classNamePrefix="select"
                            options={technosData}  
                            onChange={setTechnos}
                            key={technosData._id}                         

                        >
                            {/* <options> {singleTechno} </options> */}
                        </Select>
                        {/* <Form.Control as="select" value={[technos]} onChange={(e) => setTitle(e.target.value)} multiple>
                            { technosData.map( t => (
                                <option key={t._id} value={t._name} > {t.name} </option>
                            ))}
                        </Form.Control>                         */}
                    </Form.Group>

                        <Button variant="success" type="submit">
                            Create
                        </Button>
                        <Button variant="danger" type="submit" onClick={handleClose}>
                            Cancel
                        </Button>
                    </Form>
                </Modal.Body>                
            </Modal>

            
        </div>
    )
}

export default Admin
