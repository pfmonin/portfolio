import React from 'react'
import { Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import '../css/Hall.css'

const Hall = () => {
    return (
        <div className="hall">
            <div className="menu">
                <Link to="/project" className="hall-project  "><h2 className="hall-link">Project</h2></Link>
                <Link to="/about" className="hall-about "><h2 className="hall-link">About</h2></Link>
                <Link to="/contact" className="hall-contact "><h2 className="hall-link">Contact</h2></Link>
            </div>

            <div className="content">                
                <div className="presentation">
                    <h1 >Pierre-François Monin</h1>
                    <h2 >Full-Stack Web Developer</h2> 
                    <div className="social">
                        <a  to="https://gitlab.com/users/orou.dev/projects" alt="gitlab"><i className="fab fa-gitlab fa-2x"></i> </a>
                        <a  to="/" alt="linkedin"><i class="fab fa-linkedin fa-2x"></i></a>
                        <a  to="/contact" alt="mail"><i className="fas fa-envelope fa-2x"></i> </a>
                        <a  to="" alt="discord"><i className="fab fa-discord fa-2x"></i> </a>
                    </div> 
                </div>
                
            </div>

        </div>
    )
}

export default Hall
