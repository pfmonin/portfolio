import React from 'react'
import '../css/HomeScreen.css'
import { Link } from 'react-router-dom'
const HomeScreen = () => {


    return (
        <div className="homescreen">
            <div className="words-container">
                <h1 className="eat">eat</h1>
                <h1 className="code">Code</h1>
                <h1 className="sleep">Sleep</h1>
                <h1 className="repeat">and repeat ...</h1>   
            </div>
            <div className="enter">
                <Link to="/boudoir" className="btn-enter">ENTER</Link>
            </div>
                     
        </div>
    )
}

export default HomeScreen
