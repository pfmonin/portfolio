import React from 'react'
import '../css/Header.css'
import { Navbar, Nav, Container } from 'react-bootstrap'
import { Link } from 'react-router-dom'


const Header = () => {
    return (
        <>

        <header>

        <Navbar bg="light" expand="lg">
            <Link to="/">Portfolio</Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Link to="/">Home</Link>
                        <Link to="/project">Project</Link>                
                        <Link to="/about">About</Link>                
                        <Link to="/contact">Contact</Link>                
                    </Nav>                    
                </Navbar.Collapse>
            </Navbar>

        </header>
            
        </>
    )
}

export default Header
