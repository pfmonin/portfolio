export const technosData = [
    {
        id: 1,
        name: 'React JS',
        value: 'React JS',
        label: 'React JS',
        icon: '../img/technos/react'
    }, 
    {
        id: 2,
        name: 'Node JS',
        value: 'Node JS',
        label: 'Node JS',
        icon: '../img/technos/node'
    },
    {
        id: 3,
        name: 'MongoDB',
        value: 'MongoDB',
        label: 'MongoDB',
        icon: '../img/technos/mongo'
    }, 
    {
        id:4,
        name: 'Express',
        icon: '../img/technos/express'
    }, 
    {
        id: 5,
        name: 'Bootstrap',
        icon: '../img/technos/bootstrap'
    }, 
    {
        id: 6,
        name: 'JQuery',
        icon: '../img/technos/jquery'
    }, 
    {
        id: 7,
        name: 'Symfony',
        icon: '../img/technos/symfony'
    }, 
    {
        id:8,
        name: 'Twig',
        icon: '../img/technos/twig'
    }, 
    {
        id:9,
        name: 'Laravel',
        icon: '../img/technos/laravel'
    }, 
    {
        name: 'Wordpress',
        icon: '../img/technos/wordpress'
    }, 
    {
        name: 'WooCommerce',
        icon: '../img/technos/woocommerce'
    }, 
    {
        name: 'Dokan',
        icon: '../img/technos/dokan'
    }, 
    {
        name: 'Prestashop',
        icon: '../img/technos/prestashop'
    }, 
    {
        name: 'React JS',
        icon: '../img/technos/react'
    }, 
    {
        name: 'Stripe',
        icon: '../img/technos/stripe'
    },
    {
        name: 'PayPal',
        icon: '../img/technos/paypal'
    }
];

