import Axios from 'axios'

function findAll () {
    return Axios
    .get("http://localhost:8080/projects/")
    .then(response => response.data)
}

export default {
    findAll,
};
