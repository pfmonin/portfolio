const mongoose = require("mongoose")


const TechnoSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }, 
    version: {
        type: String
    },
    icon: {
        type: String
    }
})

const TaskSchema = mongoose.Schema({
    title: { 
        type: String
    },
    description: {
        type: String
    }
})


const ProjectSchema = mongoose.Schema({

    title: {
        type: String,
        required: true,
        unique: true         
    },
    subtitle: {
        type: String
    },
    description: {
        type: String,
        required: true
    },
    coverImg: {
        type: String,
        default: "../img/cover.JPG", 
    }, 
    techno: [ TechnoSchema ], 
    task: [ TaskSchema ]
})

module.exports = mongoose.model('Project', ProjectSchema);
