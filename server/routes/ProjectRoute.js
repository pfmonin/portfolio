const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const Project = require('../models/ProjectModel')
const Task = require('../models/ProjectModel')
const Techno = require('../models/ProjectModel')


// Method : GET => Find all project
router.get('/', async(req, res) => {
    try {
        const fetchedProject = await Project.find({})
        res.status(200).json(fetchedProject)
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
})

// Method : GET => Find one project
router.get('/:id', async(req, res) => {
    try {
        id = req.params.id;        
        const fetchOne = await Project.findById(id)
        res.status(200).json(fetchOne)
    } catch (error) {
        res.status(500).json({ error: error.message })
    }
})

// Method : POST => Create a project
router.post('/create', async(req, res) => {
    try {
        const { title, subtitle, description, coverImg, techno, task } = req.body
        if ( !title || !description )
            return res.json({message : "Title and description must be filled !"})        

        const newProject = new Project({
            title, 
            subtitle, 
            description,
            coverImg, 
            techno, 
            task
        })
        const savedProject = await newProject.save()
        res.status(200).json(savedProject)
    } catch (error) {
        res.status(500).json({error: error.message})        
    }
}) 


module.exports = router;