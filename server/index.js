const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
const mongodb = require("mongodb")
require("dotenv/config")
const User = require("./models/UserModel")
const Project = require('./models/ProjectModel')
// const Technos = require('./models/ProjectModel')


const app = express()
const router = express.Router()
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(cors()); 

// routes
const UserRoutes = require('./routes/UserRoute');
app.use('/users', UserRoutes);
const ProjectRoutes = require('./routes/ProjectRoute');
app.use('/projects', ProjectRoutes);
// const TechnoRoutes = require('./routes/TechnoRoute')
// app.use('/technos', TechnoRoutes)


const mongoDB = process.env.DB_CONNECTION;
mongoose.connect(mongoDB, { 
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useCreateIndex: true
    },
    (req, res) => {
    console.log('Connection to MongoDB up');
});
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error"));

app.listen(8080, () => {
    console.log('server listening at port 8080')
}); 





